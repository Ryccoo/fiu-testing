---
title: Poster for 8 Faces
scripts: Latin
typefaces: Skolar
country: United Kingdom
year: 2011
---

[8 Faces magazine](http://8faces.com/) conceived a limited edition of eight A3 (297mm x 420mm) artwork prints. David Březina designed print #1 using his typeface Skolar. The poster is the logical next step from the promotional poster designed for TypeTogether. While the first poster is about a single typeface and its features, this poster is about typeface design in general. It describes the basics behind “proportions”, “structures”, “modulation”, and “rhythm” in type design.
