---
title: Bobulate
scripts: Latin
typefaces: Skolar
country: USA
year: 2011
---

[Bobulate](http://bobulate.com) is a blog by Liz Danzico. The simple and clean site design was masterfully accomplished by Liz’s close friend Jason Santa Maria. Skolar is used for the masthead and the text throughout the blog. The blog also has an evening edition!
