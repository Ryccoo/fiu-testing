---
title: CAS Linguistics edition
scripts:
  - Latin
  - Cyrillic
  - Greek
typefaces: Skolar
country: Czech Republic
year: 2010–2014
---

An established series of books on Linguistics by the etymological department of The Academy of Sciences of the Czech Republic were redesigned by Vít Boček and David Březina in 2010. Notably, an extended custom version of Skolar was developed in six weights in order to typeset the books efficiently.

The generous layouts count on the complexity of academic texts, great amount of footnotes, and readers who have the urge to make notes while reading. The cover plays with the idea of boustrophedon writing as a reference to antiquity, the age in which everything had been said already!
