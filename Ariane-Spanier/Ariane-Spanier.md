---
title: Ariane Spanier
scripts: Latin
typefaces: Nassim
country: Austria
year: 2011
---

Typopassage is a public-space exhibition dedicated to type and lettering. It is hosted by the Museums Quartier in Vienna and the shows are curated by renowned Austrian design office bauer Konzept und Gestaltung. In the accompanying catalogue to Typopassage no. 2, designed by German designer Ariane Spanier, Nassim has been used to great effect.
