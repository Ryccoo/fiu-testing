---
title: 804 booklets
scripts: Latin
typefaces: Skolar
country: Germany
year: 2011
---

Booklets by German design studio [achtnullvier](http://achtnullvier.com/) deal with various aspects of Graphic design. Presented here is a ‘Naming’ booklet, which deals with naming/name-finding, and ‘Semiotic’ booklet, which provides a brief introduction to semiotics, the general theory of signs and sign systems. Designed by Helge Rieder, Carsten Prenger, Oliver Henn – with our Skolar and Taz by Lucas de Groot.

Photos by 804© Graphic Design.
