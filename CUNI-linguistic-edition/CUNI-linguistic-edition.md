---
title: Charles University Linguistics edition
scripts:
  - Latin
  - Greek
typefaces: Skolar
country: Czech Republic
year: 2013
---

Charles University in Prague is publishing a series focussing on Linguistics. The book covers have been designed by [František Štorm](http://stormtype.com), the contents have been typeset by studio [Lacerta](http://sazba.cz). Skolar PE has been put to great use in these highly structured academic texts.
