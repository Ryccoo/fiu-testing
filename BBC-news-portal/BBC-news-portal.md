---
title: BBC news portal
scripts: Arabic
typefaces: Nassim
country: United Kingdom
year: 2011
---

Following the redesign of the mother-page of the BBC News, [BBC Arabic](http://bbc.co.uk/arabic) and [BBC Persian](http://bbc.co.uk/persian) launched their redesigned websites. These are probably the first, and certainly the biggest websites yet to use webfonts – Arabic webfonts that is. The BBC uses custom versions of Nassim by Titus Nemeth. The typeface was painstakingly optimised for screen and for the web with the help of Thomas Grace (hinting) and Tim Ahrens (webfonts production). The typeface was further redesigned for the BBC Persian. Titus redrew Nassim to give the website a distinctly Iranian look, while maintaining coherence with the BBC-brand visual language. Read more about the process on [Rosetta blog](http://rosettatype.com/blog/2011/04/02/BBC-Arabic).
