---
title: Die Mühen der Ebenen
scripts: Latin
typefaces: Skolar
country: Poland
year: 2013
---

This hardbound book, published by Wydawnictwo Naukowe WSPiA, contains essays on contemporary German literature since 1989. The authors seek answers to the question: did the union euphoria, the association disappointment, the East German "ostalgia", the western apolitical, sometimes defeatist attitude, a retreat into the personal sphere of the 90s, give way to newer perspectives? Book design by Bartłomiej Bączkowski.
