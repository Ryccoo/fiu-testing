---
title: … und dann war alles anders.
scripts:
  - Latin
  - Cyrillic
typefaces: Skolar
country: Austria
year: 2013
---

The book features stories of decisive moments in the life of 39 different people from around the world who made Austria their home. The main German text is spiced up with quotes in the protagonists’ languages. Designer Stefan Gormász solved this “typographic babylon” mostly with Skolar and several other typefaces (e.g. for Korean or Arabic).
