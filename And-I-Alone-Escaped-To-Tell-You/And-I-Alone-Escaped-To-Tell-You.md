---
title: And I Alone Escaped To Tell You
scripts: Latin
typefaces: Huronia
country: Canada
year: 2014
---

This collection of poems blends documentary material, memory, experience, and imagination to evoke the lives of the early Black Nova Scotians, and the generations that followed. The author, Sylvia Hamilton is a filmmaker and award-winning writer, who presents a moving meditation that connects us to the African exodus. The book was designed and typeset by Andrew Steeves in a typeface Huronia by Ross Mills and printed offset at [Gaspereau Press](http://www.gaspereau.com/meetthepress.php). 


