---
title: DNA to Darwin
scripts: Latin
typefaces: Skolar
country: United Kingdom
year: 2011
---

[DNA to Darwin](http://www.dnadarwin.org/) allows 16–19 year-old school students to explore the molecular evidence for evolution. A project by the University of Reading, the website and the associated written materials were designed by Jon Hicks of [Hicksdesign](http://www.hicksdesign.co.uk/).